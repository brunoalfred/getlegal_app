part of 'leads_bloc.dart';



enum LeadStatus { initial, success, failure }

class LeadState extends Equatable {
  const LeadState({
    this.status = LeadStatus.initial,
    this.leads = const <Lead>[],
    this.hasReachedMax = false,
  });

  final LeadStatus status;
  final List<Lead> leads;
  final bool hasReachedMax;

  LeadState copyWith({
    LeadStatus? status,
    List<Lead>? leads,
    bool? hasReachedMax,
  }) {
    return LeadState(
      status: status ?? this.status,
      leads: leads ?? this.leads,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() {
    return '''PostState { status: $status, hasReachedMax: $hasReachedMax, posts: ${leads.length} }''';
  }

  @override
  List<Object> get props => [status, leads, hasReachedMax];
}
