import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:getlegal_app/features/leads/presentation/bloc/leads_bloc.dart';
import 'package:getlegal_app/features/leads/presentation/presentation.dart';

import 'package:http/http.dart' as http;

class LeadsPage extends StatelessWidget {
  const LeadsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Leads')),
      body: BlocProvider(
        create: (_) => LeadBloc(httpClient: http.Client())..add(LeadFetched()),
        child: LeadsList(),
      ),
    );
  }
}