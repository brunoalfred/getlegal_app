import 'package:equatable/equatable.dart';

class Lead extends Equatable {
  const Lead({
    required this.id,
    required this.title,
    required this.name,
    required this.company,
  });

  final String id;
  final String title;
  final String name;
  final String company;

  @override
  List<Object> get props => [id, title, name, company];
}
