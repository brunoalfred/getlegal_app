import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:getlegal_app/features/leads/data/models/lead.dart';
import 'package:http/http.dart' as http;
import 'package:stream_transform/stream_transform.dart';

part 'leads_event.dart';
part 'leads_state.dart';

const _postLimit = 20;
const throttleDuration = Duration(milliseconds: 100);

// disable spamming the API with many request at a time
EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class LeadBloc extends Bloc<LeadEvent, LeadState> {
  LeadBloc({required this.httpClient}) : super(const LeadState()) {
    on<LeadFetched>(
      _onLeadFetched,
      transformer: throttleDroppable(throttleDuration),
    );
  }

  final http.Client httpClient;

  Future<void> _onLeadFetched(
      LeadFetched event, Emitter<LeadState> emit) async {
    if (state.hasReachedMax) return;
    try {
      if (state.status == LeadStatus.initial) {
        final leads = await _fetchLeads();
        return emit(state.copyWith(
          status: LeadStatus.success,
          leads: leads,
          hasReachedMax: false,
        ));
      }
      final leads = await _fetchLeads(state.leads.length);
      emit(leads.isEmpty
          ? state.copyWith(hasReachedMax: true)
          : state.copyWith(
              status: LeadStatus.success,
              leads: List.of(state.leads)..addAll(leads),
              hasReachedMax: false,
            ));
    } catch (_) {
      emit(state.copyWith(status: LeadStatus.failure));
    }
  }

  Future<List<Lead>> _fetchLeads([int startIndex = 0]) async {
    final response = await httpClient.get(
      Uri.parse('https://demo-new.getlegal.co.tz/api/leads'),
      headers: {
        // set api key with key = authtoken and value = your api key
       'authtoken': '<value>'
      },
    );


    
    if (response.statusCode == 200) {
      final body = json.decode(response.body) as List;
      
      try {
        return body.map((dynamic json) {
          return Lead(
            id: json['id'] as String,
            title: json['title'] as String,
            name: json['name'] as String,
            company: json['company'] as String,
          );
        }).toList();
      } catch (e) {
        print(e);
        debugPrint(e.toString());
      }
    }
    
    throw Exception('error fetching posts');
  }
}
