import 'package:flutter/material.dart';
import 'package:getlegal_app/features/leads/data/models/lead.dart';

class LeadListItem extends StatelessWidget {
  const LeadListItem({Key? key, required this.lead}) : super(key: key);

  final Lead lead;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Material(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6),
        child: ListTile(
          leading: Text('${lead.id}', style: textTheme.caption),
          title: Text(lead.name),
          isThreeLine: true,
          subtitle: Text(lead.company),
          dense: true,
        ),
      ),
    );
  }
}
