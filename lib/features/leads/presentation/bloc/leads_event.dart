part of 'leads_bloc.dart';



abstract class LeadEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LeadFetched extends LeadEvent {}
