import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:getlegal_app/features/leads/presentation/bloc/leads_bloc.dart';
import 'package:getlegal_app/features/leads/presentation/presentation.dart';
import 'package:getlegal_app/features/leads/presentation/widgets/lead_item.dart';

class LeadsList extends StatefulWidget {
  @override
  _LeadsListState createState() => _LeadsListState();
}

class _LeadsListState extends State<LeadsList> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LeadBloc, LeadState>(
      builder: (context, state) {
        switch (state.status) {
          case LeadStatus.initial:
            return const Center(child: CircularProgressIndicator());
          case LeadStatus.failure:
            return const Center(child: Text('failed to fetch Leads'));
          case LeadStatus.success:
            if (state.leads.isEmpty) {
              return const Center(child: Text('no leads'));
            }
            return ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                return index >= state.leads.length
                    ? BottomLoader()
                    : LeadListItem(lead: state.leads[index]);
              },
              itemCount: state.hasReachedMax
                  ? state.leads.length
                  : state.leads.length + 1,
              controller: _scrollController,
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(height: 3),
            );
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) context.read<LeadBloc>().add(LeadFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
